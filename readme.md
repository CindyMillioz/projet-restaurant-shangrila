# Projet - Restaurant Shangrila

## Tech
- HTML
- CSS
- Bootstrap

## Consignes :
- Réaliser une page web selon une maquette existante
- Faire une bonne architecture de projet
- Utiliser HTML/CSS
- Utiliser Bootrstrap
- Faire un site responsive

## Architecture
- index.html
- readme.md
- image :
    - 1.jpg
    - 2.jpg
    - 3.jpg
    - 4.jpg
    - 5.jpg
    - 6.jpg
    - 7.jpg
    - 8.jpg
    - cards.png
    - chef-1.png
    - chef-2.png
    - chef-3.png
    - chef-thumb.jpg
    - delicious_1.jpg
    - delicious_2.jpg
    - delicious_3.jpg
    - delicious_4.jpg
    - fond2.jpg
    - image-1.jpg
    - image-2.jpg
    - logo.png
    - news-1.jpg
    - news-2.jpg
    - news-3.jpg
    - separater.png

- 

